so the idea is:
PF opens the 'penmount' program, and reads events in the form 
(pen-press <x> <y>) and (pen-motion <x> <y>)

then the penmount.pf driver will convert this to window coordinates (using 
'calibration.pf' wich is a pf script generated using 'get-calibration.pf') 
and send it to the 'responder' word. at that point, events could be captured 
by your standard opengl event responder.

make sure you run 'make' to create the 'driver' executable

calibration.pf		calibration data (generated by get-calibration.pf)
get-calibration.pf	calculate calibration
driver.c		convers raw serial bytes to list (s-expression)
dump			script to raw dump serial port
penmount		sets serial port config + execs 'driver'
penmount.pf		pf driver wrapper, uses calibration.pf
serial.Linux		set serial port confguration
