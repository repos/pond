#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>


/* the initialization from xorg driver doesnt seem to work
   correctly. most probably i'm doing something wrong, so it's
   disabled for now. apparently the device sends a predictable
   pattern of 0xBF and 0xFF
 

 0xFF = motion
 0xBF = state change

 this is enough to get motion and press events for PF.

 */


int fd = -1;
unsigned char buf[5];
#define STATUS buf[0]

unsigned char LAST_STATUS = 0xBF;

#define MESSAGE(format...) { printf(format); fflush(stdout); }
#define ERROR(format...)   { MESSAGE("# " format); exit(1); }

// debug disabled
// #define DEBUG(format...)   MESSAGE("# " format)
#define DEBUG(format...) { }


/* take 7 bits from each byte */
unsigned int word(unsigned int high, unsigned int low){
    return  (low & 0x7F) | ((high & 0x7F) << 7) ;
}

void read_status(void)  { read(fd, buf, 1); }
void read_data(int n)   { read(fd, buf+1, n); }

void print_packet_raw(void) {
    MESSAGE("(0x%02x %d %d)\n",
	    buf[0],
	    word(buf[1], buf[2]),
	    word(buf[3], buf[4]));
}

int current_change(void)  { return !(STATUS & 0x40); }
int last_change(void)     { return !(LAST_STATUS & 0x40); }

void print_packet_symbolic(void){
    char *tag = (current_change() && last_change()) ?
	"pen-press" : "pen-motion";

    MESSAGE("(%s %d %d)\n",
	    tag,
	    word(buf[1], buf[2]),
	    word(buf[3], buf[4]),
	    LAST_STATUS);

}

void zero_packet(void){
    memset(buf, 0, 5);
}

/* data packets are 5 bytes */
void read_packet(void){
    LAST_STATUS = STATUS;
    zero_packet();
    for(;;){
	read_status();
	DEBUG("status %02x\n", STATUS);


	if (0xBF == (STATUS & 0xBF)){ 
	    DEBUG("that's valid\n");
	    break;
	}
    }
    read_data(4);
}

/* response packets are 3 bytes */
void read_response(void){
    for(;;){
	read_status();
	if (0xF2 == STATUS){
	    printf("got proper status response\n");
	    break;
	}
	DEBUG("read_response() got %02x retry\n", STATUS);
	//sleep(1);
    }
    read_data(2);
}

void write_packet(unsigned char *packet){
    write(fd, packet, 5);
}

void init_device(void){
    unsigned char packet[] = {0xF2,0,0,0,0};
    write_packet(packet);
    read_response();
    if ((buf[1] == 0xD9) &&
	(buf[2] == 0x0A)) {
	packet[0] = 0xF1;
	write_packet(packet);
	DEBUG("init OK\n");
    }
    else {
	ERROR("got wrong init response\n");
    }
}




int main(int argc, char **argv){
    fd = open(argv[1], O_RDWR);
    // DEBUG("initializing device\n");
    // init_device();
    DEBUG("entering event loop\n");
    for(;;){
	read_packet();
	//print_packet_raw();
	print_packet_symbolic();
    }
}
